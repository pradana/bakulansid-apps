import 'dart:io';

import 'package:flutter/material.dart';
import 'package:webview_flutter/webview_flutter.dart';

class Webv extends StatefulWidget {
  @override
  WebvState createState() => WebvState();
}

class WebvState extends State<Webv> {
  @override
  void initState() {
    super.initState();
    // Enable hybrid composition.
    if (Platform.isAndroid) WebView.platform = SurfaceAndroidWebView();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        body: SafeArea(
          child: WebView(
            initialUrl: 'https://bakulansid.com',
          ),
        ),
      ),
    );
  }
}